#ifndef __BASH_CMD_H__
#define __BASH_CMD_H__

#include "stdint.h"
#include <QString>

class QBashCmd
{
    QString stripName;

public:
    QBashCmd(const QString *stripName);
    void cp(const QString &srcName,
            const QString &targetName) const;
    void strip(const QString &inName, const QString &outName) const;
    void md5sum(const QString &inName, const QString &outName) const;
    void targz(const QString &workDirectory, const QString &inName,
               const QString &outName,
               const QString &sumName) const;
    void _7z(const QString &inName, const QString &outName) const;
    void rm(const QString &inName) const;

};

#endif//__BASH_CMD_H__
