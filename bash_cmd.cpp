#include "bash_cmd.h"

#include <QProcess>
#include <QStringList>


QBashCmd::QBashCmd(const QString *stripName)
{
    if(nullptr != stripName)
    {
        this->stripName=*stripName;
    }
}

void QBashCmd::cp(const QString &srcName, const QString &targetName) const
{
    QProcess process;
    QString program("cp");
    QStringList arguments;
    arguments << "-f" << srcName << targetName;
    process.start(program, arguments);
    process.waitForFinished();
}

void QBashCmd::strip(const QString &inName, const QString &outName) const
{
    if(nullptr == stripName)
    {
        return;
    }
    QProcess process;
    QString program(stripName);
    QStringList arguments;
    arguments << inName << "-o" << outName;
    process.start(program, arguments);
    process.waitForFinished();
}

void QBashCmd::md5sum(const QString &inName, const QString &outName) const
{
    QProcess process;
    QString program("md5sum");
    QStringList arguments;
    arguments << inName;// << ">" << outName;
    process.setStandardOutputFile(outName); //readAll();
    process.start(program, arguments);
    process.waitForFinished();
}

void QBashCmd::targz(const QString &workDirectory,
                     const QString &inName,
                     const QString &outName,
                     const QString &sumName) const
{
    QProcess process;
    QString program("tar");
    QStringList arguments;
    arguments << "-czf" << outName << inName <<sumName;
    process.setWorkingDirectory(workDirectory);
    process.start(program, arguments);
    process.waitForFinished();
}

void QBashCmd::_7z(const QString &inName, const QString &outName) const
{
    QProcess process;
    QString program("7z");
    QStringList arguments;
    arguments << "a" << outName << inName;
    process.start(program, arguments);
    process.waitForFinished();
}

void QBashCmd::rm(const QString &inName) const
{
    QProcess process;
    QString program("rm");
    QStringList arguments;
    arguments << inName;
    process.start(program, arguments);
    process.waitForFinished();
}


