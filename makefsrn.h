#ifndef MAKEFSRN_H
#define MAKEFSRN_H

#include <stdint.h>
#include <QString>
#include <QDateTime>
#include <QByteArray>
#include "bash_cmd.h"

class MakeFsrn
{
    QBashCmd *bashCmd;

public:
    void create(QBashCmd &bashCmd);

    bool makeFirmware(const QString &appName,
                      const QString &firmwareName,
                      const QString &salnName,
                      bool strip);
};

#endif // MAKEFSRN_H
