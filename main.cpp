#include <QCoreApplication>
#include <QCommandLineParser>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QTextCodec>
#include <QDebug>
#include <stdio.h>
#include "bash_cmd.h"
#include <QFile>
#include <QDir>
#include <QList>


//static const char *stripName=
//        "/opt/SDK-PD17.1.2/sysroots/x86_64-phytecsdk-linux/usr/bin/arm-phytec-linux-gnueabi/arm-phytec-linux-gnueabi-strip";

static
QString extractNameAndPath(const QString &fullPath, QString &path)
{
    int index=fullPath.lastIndexOf('/');
    path="./";
    if(0>index) return fullPath;


    path=fullPath.mid(0, index+1);
    return fullPath.mid(index+1);
}

static
bool makeFirmware(const QString &appName,
                  const QString &firmwareName,
                  const QString &salnName,
                  const QString *stripUtilityName)
{
    QBashCmd bashCmd(stripUtilityName);//("arm-phytec-linux-gnueabi-strip");

    QString workDirectory("./");
    extractNameAndPath(firmwareName, workDirectory);

    QString path;
    QString stripName=extractNameAndPath(appName, path) + ".strip";
    QString sumName(stripName+".md5");
    QString targzName(stripName + ".tar.gz");

    QDir::setCurrent(workDirectory);

    //# delete old work files
    bashCmd.rm(stripName);
    bashCmd.rm(sumName);
    bashCmd.rm(targzName);

    //# strip debug info
    if(nullptr==stripUtilityName)
    {//Debug - ������ �������� ����
        bashCmd.cp(appName, stripName);
    }
    else
    {//Release - �������� ���������� ����������
        bashCmd.strip(appName, stripName);
    }
    //# make archive
    bashCmd.md5sum(stripName, sumName);
    bashCmd.targz(workDirectory, stripName, targzName, sumName);
    //# make version
    bashCmd.rm(firmwareName);
    bashCmd.cp(targzName, firmwareName);
    //# make ����
    bashCmd.rm(salnName);
    bashCmd._7z(firmwareName, salnName);
    //# remove temporary files
    bashCmd.rm(stripName);
    bashCmd.rm(sumName);
    bashCmd.rm(targzName);

    return true;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("make-ksu-firmware");
    QCoreApplication::setApplicationVersion("1.0.2");


    QCommandLineParser parser;
    parser.setApplicationDescription("KSU-6.05 firmware maker");
    parser.addHelpOption();
    parser.addVersionOption();

    //������� ����
    parser.addPositionalArgument("source", "Source file name");
    //���������� ����������
    parser.addPositionalArgument("destination-folder", "Destination Folder");
    //�������
    QCommandLineOption helpOption(QStringList() << "h" << "help", "Help about the program ", "help",
    "./make-ksu-firmware ksu ./ -d 1 -s 13.1.36 -z 27");
    //��� strip-�������
    QCommandLineOption stripUtilityOption(QStringList() << "u" << "strip-utility", "Strip utility name", "utility", "arm-phytec-linux-gnueabi-strip");
    parser.addOption(stripUtilityOption);
    //������ ������������
    QCommandLineOption distVersionNameOption(QStringList() << "d" << "dist-version", "Distributive Version", "dist", "1");
    parser.addOption(distVersionNameOption);
    //������ �����
    QCommandLineOption softVersioNameOption(QStringList() << "s" << "soft-version", "Software Version", "version", "xx.xx.xx");
    parser.addOption(softVersioNameOption);
    //����� ���������
    QCommandLineOption izmNumberOption(QStringList() << "z" << "izm-number", "Izm. Number", "izmnum", "0");
    parser.addOption(izmNumberOption);


    //������ ��������� � �����
    parser.process(a);
    //����� ������ ������� ���������?
    if(parser.isSet(helpOption))
    {
        qDebug() << "example: " << parser.value(helpOption);
        return 0;
    }

    int exitCode=1;
    while(true)
    {
        //��������
            //������� ����
        const QStringList arguments=parser.positionalArguments();
        QString inputFileName("ksu");
        if(1>arguments.count())
        {
            qDebug() << "Error: Enter input file name";
            break;
        }
        inputFileName=arguments.at(0);
            //���������� ����������
        QString destinationFolderName("./");
        extractNameAndPath(inputFileName, destinationFolderName);
        if(2<=arguments.count())
            destinationFolderName=arguments.at(1);
        if('/'!=destinationFolderName.at(destinationFolderName.length()-1))
            destinationFolderName+='/';
        //�������� �����
        //strip
        QString stripUtilityName("");
        QString *pStripUtilityName = nullptr;
        if(parser.isSet(stripUtilityOption))
        {
            stripUtilityName = parser.value(stripUtilityOption);
            pStripUtilityName = &stripUtilityName;
        }
        //����� ������������ ��� ������� ��������� ������� ����
        QString distVersionString=parser.value(distVersionNameOption);
        bool ok;
        distVersionString.toInt(&ok);
        if(false==ok)
        {
            qDebug() << "Error: Failue distributive version: \"" << distVersionString << "\"";
            break;
        }
        //����� ������ ��
        QString softVersioName=parser.value(softVersioNameOption);
        //����� ��������� � �������� ����-�
        QString izmNumberString=parser.value(izmNumberOption);
        izmNumberString.toInt(&ok);
        if(false==ok)
        {
            qDebug() << "Error: Failue Izm. number: \"" << izmNumberString << "\"";
            break;
        }

        qDebug() << "Info: Wait 10 seconds...";

        QString firmwareName(destinationFolderName + softVersioName+'.'+distVersionString+".ksu");//"13.1.1912.1.ksu"
        QTextCodec *codec = QTextCodec::codecForName("CP1251");
        QString salnName=codec->toUnicode("����.00138-90");
        salnName+=codec->toUnicode("_���");
        salnName+=izmNumberString;
        salnName+=".7z";

        if(false == makeFirmware( inputFileName,
                                  firmwareName,
                                  salnName,//"����.00138-90_���1.7z"
                                  pStripUtilityName))
        {
            qDebug() << "Error: Can't create ksu-firmware";
            break;
        }
        qDebug() << "Info: Create ksu-firmware successfully";

        exitCode=0;
        break;
    }

    return exitCode;
}
